odoo-openupgrade-wizard changes
*******************************

This file compiles releases and changes made in
``odoo-openupgrade-wizard``.

.. towncrier release notes start

odoo-openupgrade-wizard 0.7.0 (2024-05-02)
==========================================

Features
--------

- Add ``--config-file`` and ``--modules-file`` CLI options. This allows to use
  different files than the default ones. This is useful when using the same
  environment for different databases. (add-config-file-cli-option)
- Add database name to container name and publish Docker ports only when needed
  to allow to upgrade multiple databases in parallel.
  (allow-to-upgrade-multiple-databases-in-parallel)
- Drop support for python version < 3.9. Update dependencies and fix some
  issue liked to that. (drop-old-python-support)
- Add a new option ``--postgresql-version`` in ``oow init`` command to
  define the version of the postgresql image to be used for the project.
  (option-postgresql-version)
- Factorize code. Allways set --log-level=DEBUG in tests.
  (set-log-level-debug-default-in-cli_runner_invoke)


Bugfixes
--------

- Allow hyphen-minus character in database names.
  (allow-minus-in-database-names)


odoo-openupgrade-wizard 0.6.0 (2024-03-20)
==========================================

Features
--------

- Add ``dropdb`` command to easily delete database and filestore of existing
  Odoo databases in PostgreSQL container. (add-dropdb)
- With ``install-from-csv`` and the ``--with-demo / --without-demo``, you
  can control if the created database will be populated with demo data or
  not. (add-install-from-csv-demo)
- Add ``restoredb`` command to restore database and filestore in the
  PostgreSQL container. (add-restoredb)
- Add ``--update-modules`` option to ``run`` command. (imp-run-update-modules)
- ``run`` and ``upgrade`` command are now harmonized with the option
  ``--with-demo / --without-demo``. By default demo data is always false.
  (imp-run-upgrade)


Bugfixes
--------

- ``copydb`` now copy also the filestore of the database. (copydb-filestore)
- Fix warning message for ``estimate-workload`` command.
  (fix-estimate-workload-warning-message)
- Fix getting url on apps.odoo.com that prevent from running
  ``estimate-workload`` command. (fix-getting-url)
- Fix crash when a addons-path directory does not contain modules.
  Directory that does not contains odoo modules are now removed from
  addons-path option of odoo. (fix-repo)


Misc
----

- ci-improvement
